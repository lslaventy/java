package Glava11;

import javax.sound.midi.*;

public class MusicTest1 {
    public void play(){
        try{
            Sequencer sequencer = MidiSystem.getSequencer();    //создаем ноый синтезатор
            System.out.println("Мы получили синтезатор");   //сообщаем о получении нового синтезатора
        }catch(MidiUnavailableException ex){
            System.out.println("Неудача");  //сообщаем о не получении нового синтезатора
        }
    }//закрываем play
    public static void main(String[] args) {
        MusicTest1 mt = new MusicTest1();   //создаем наше приложение
        mt.play();  //запускаем синтезатор
    }//закрываем main
}//закрываем class
