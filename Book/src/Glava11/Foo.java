package Glava11;

public class Foo {
    public void go(){
        Laundry laundry = new Laundry();
        try{
            laundry.doLoundry();
        }catch(PantsException pex){
            System.out.println("Пропали брюки");
        }catch(LingerieException lex){
            System.out.println("Пропало дамское белье");
        }catch(ClothingException cex){
            System.out.println("Пропала вся одежда");
        }
    }
    public static void main(String[] args) {
        Foo f = new Foo();
        f.go();
    }
}
