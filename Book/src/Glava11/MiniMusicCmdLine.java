package Glava11;

import javax.sound.midi.*;

public class MiniMusicCmdLine {
    public static void main(String[] args) {
        MiniMusicCmdLine mini = new MiniMusicCmdLine(); //создаем приложение
        if(args.length < 2){    //если в аргументах меньше 2-х аргументов
            System.out.println("Не забудьте аргументы для инструмента и ноты"); //указываем на необходимость добавления 2-х аргументов
        }else {
            int instrument = Integer.parseInt(args[0]); //парсим первый аргумент как инструмент
            int note = Integer.parseInt(args[1]);   //парсим второй аргумент как ноту
            mini.play(instrument, note);    //передаем параметры в функцию для проигрывания
        }
    }//конец main

    public void play(int instrument, int note){
        try{
            Sequencer player = MidiSystem.getSequencer();   //получаем синтезатор
            player.open();  //открываем синтезатор как поток
            Sequence seq = new Sequence(Sequence.PPQ, 4);   //создаем последовательность - диск
            Track track = seq.createTrack();    //создаем трек на нашей последовательности - диске

            MidiEvent event = null; //создаем пустое событие

            ShortMessage first = new ShortMessage();    //формируем сообщение
            first.setMessage(192, 1, instrument, 0);    //заполняем сообщение
            MidiEvent changeInstrument = new MidiEvent(first, 1);   //заполняем событие сообщением
            track.add(changeInstrument);    //добавляем событие в последовательность

            ShortMessage a = new ShortMessage();    //формируем сообщение
            a.setMessage(144, 1, note, 100);    //заполняем сообщение
            MidiEvent noteOn = new MidiEvent(a, 1); //заполняем событие
            track.add(noteOn);  //добавляем событие в последовательность

            ShortMessage b = new ShortMessage();    //формируем сообщение
            b.setMessage(128, 1, note, 100);    //заполняем сообщение
            MidiEvent noteOff = new MidiEvent(b, 16);   //заполняем событие
            track.add(noteOff); //добавляем событие в последовательность

            player.setSequence(seq);    //устанавливаем последовательность в синтезатор
            player.start(); //включаем синтезатор на воспроизведение

        }catch (Exception ex){  //обрабатываем исключение
            ex.printStackTrace();
        }
    }
}
