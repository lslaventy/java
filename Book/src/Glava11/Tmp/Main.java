package Glava11.Tmp;

public class Main {
    public static void main(String[] args) {
        Integer [] massive = {1, 2, 3};
        System.out.println("Обращение за пределы массива");
        try{
            System.out.println(massive[2]);
            System.out.println("Строка следующая за вызовом из за предела массива");
        }catch(ArrayIndexOutOfBoundsException ex){
            System.out.println("Индекс вышел за границы массива");
        }finally{
            System.out.println("блок выполняется всегда");
        }
        System.out.println("Продолжение программы");
        
    }
}
