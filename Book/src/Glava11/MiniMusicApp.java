package Glava11;

import javax.sound.midi.*;

public class MiniMusicApp {
    public static void main(String[] args) {
        MiniMusicApp mini = new MiniMusicApp(); //формирование приложения
        mini.play();    //запуск приложения
    }
    public void play(){
        try{
            Sequencer player = MidiSystem.getSequencer();   //получаем синтезатор
            player.open();  //открываем синтезатор
            
            Sequence seq = new Sequence(Sequence.PPQ, 4);   //создаем новую последовательность

            Track track = seq.createTrack();    //в этой последовательности создаем трек
            ShortMessage changeInstrument = new ShortMessage(); //формируем новое сообщение
            changeInstrument.setMessage(192, 1, 102, 0);    //записываем новое сообщение
            MidiEvent me = new MidiEvent(changeInstrument, 1);  //формируем новое событие
            track.add(me);  //добавляем событие на трек

            ShortMessage a = new ShortMessage();    //формируем новое сообщение
            a.setMessage(144, 1, 44, 100);  //записываем новое сообщение
            MidiEvent noteOn = new MidiEvent(a, 1); //формируем новое событие
            track.add(noteOn);  //добывляем на трек событие

            ShortMessage b = new ShortMessage();    //формируем новое сообщение
            b.setMessage(128, 1, 44, 100);  //записываем новое сообщение
            MidiEvent noteOff = new MidiEvent(b, 16);   //формируем новое событие
            track.add(noteOff); //добавляем событие в трек

            player.setSequence(seq);    //вставляем получившуюся последовательность - диск в синтезатор

            player.start(); //включаем воспроизведение
        }catch(Exception ex){
            ex.printStackTrace();   //обработка инсключеия
        }
    }//закроем play
}//закрываем class
