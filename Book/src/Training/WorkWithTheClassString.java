package Training;

public class WorkWithTheClassString {
    public void string(){
        //демонстрация поля класса
        //доступ к компаратору через поле содержащее ссылку на данный объект
        String.CASE_INSENSITIVE_ORDER.compare("a", "s");
        String.CASE_INSENSITIVE_ORDER.equals("as");


        //Конструкторы
        String st1 ="";
        System.out.println("new String() - " + st1);

        String str = "abc";
        String st2 = "abc";
        System.out.println("new String(\"abc\") - " + st2);
        System.out.println("new String(str) - " + str);

        char[] ch1 = {'a', 'b', 'd'};
        String st3 = new String(ch1);
        System.out.println("char[] ch1 = {'a', 'b', 'd'}/new String(ch1) - " + st3);


    }
}
