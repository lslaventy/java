package Glava14;

import java.util.*;
import java.awt.event.*;
import javax.swing.*;
import java.awt.*;
import java.io.*;

public class QuizCardBuilder {
    private JTextArea question; //текстовая область вопроса
    private JTextArea answer;   //текстовая область ответв
    private ArrayList<QuizCard> cardList;   //массив указывающий на количество карточек
    private JFrame frame;   //ссылка на фрейм

    public static void main(String[] args){
        QuizCardBuilder builder = new QuizCardBuilder();    //создаем кардБилдер
        builder.go();   //запускаем его
    }

    public void go(){
        //Формируем и выводим на экран  GUI

        frame = new JFrame("Quiz Card Builder"); //подцепляем новый фрейм с заголовком
        JPanel mainPanel = new JPanel();    //создаем панель
        Font bigFont = new Font("sanserif", Font.BOLD, 24); //задаем параметры текста
        question = new JTextArea(6, 20);    //создаем поле вопроса размером 6 на 20
        question.setLineWrap(true); //перенос строки
        question.setWrapStyleWord(true);    //перенос по словам включен/по буквам выключен
        question.setFont(bigFont);  //установка шрифта в поле

        JScrollPane qScroller = new JScrollPane(question);  //ставим скрол на текстовуб панель вопроса
        qScroller.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);    //вертикальный скрол всегда есть
        qScroller.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER); //горизонтальный скрол отсутствует

        answer = new JTextArea(6, 20);  //создание поля ответ 6 на 20
        answer.setLineWrap(true);   //перенос строки
        answer.setWrapStyleWord(true);  //перенос по словам
        answer.setFont(bigFont);    //установка шрифтов

        JScrollPane aScroller = new JScrollPane(answer);    //ставим скрол на панелғ ответов
        aScroller.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);    //вертикальный скрол всегда есть
        aScroller.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER); //горизонтальный скрол отсутствует

        JButton nextButton = new JButton("Next Card");  //создание кнопки для перехода к следующей карте

        cardList = new ArrayList<QuizCard>();   //создание массива объектов карточек пока пустого

        JLabel qLabel = new JLabel("Question:");    //создаем панель с названием вопрос
        JLabel aLabel = new JLabel("Answer:");      //создаем панель с названием ответ

        mainPanel.add(qLabel);  //добавляем панель с названием вопрос
        mainPanel.add(qScroller);   //ставим облать для ввода вопроса
        mainPanel.add(aLabel);  //добавляем панель с названием ответ
        mainPanel.add(aScroller);   //ставим область для ввода ответа
        mainPanel.add(nextButton);  //ставится кнопка перехлда к следующей карточке
        nextButton.addActionListener(new NextCardListener());   //к кнопке подключаем слушателя
        JMenuBar menuBar = new JMenuBar();  //активация объекта полоска менюБар
        JMenu fileMenu = new JMenu("File"); //объявление вкладки менюБар - файл
        JMenuItem newMenuItem = new JMenuItem("Next");  //объявление части вкладки следующий
        JMenuItem saveMenuItem = new JMenuItem("Save"); //объявление части вкладки сохранение
        newMenuItem.addActionListener(new NewMenuListener());   //добавили слушателя к вкладке следующий
        saveMenuItem.addActionListener(new SaveMenuListener()); //добавили слушателя к вкладке сохранение
        fileMenu.add(newMenuItem);  //добавляем в менюБар файл - вкладку следующий
        fileMenu.add(saveMenuItem); //добавляем в менюБар файл - вкладку сохранение
        menuBar.add(fileMenu);  // теперь сам файл ставим в менбБар
        frame.setJMenuBar(menuBar); //и менбБар во фрейм
        frame.getContentPane().add(BorderLayout.CENTER, mainPanel); // общую панель действий ставим в рабочую область в центр
        frame.setSize(500, 600);    //задаем размер рабочей области
        frame.setVisible(true); //делаем фрейм видимым
    }

    public class NextCardListener implements ActionListener{
        public void actionPerformed(ActionEvent ev){
            QuizCard card = new QuizCard(question.getText(), answer.getText()); //создаем новую карточку
            cardList.add(card); //добавляем карту в список карт
            clearCard();    //сброс полей вопрос/ответ/фокус на поле вопрос
        }
    }

    public class SaveMenuListener implements ActionListener{
        public void actionPerformed(ActionEvent ev){

            QuizCard card = new QuizCard(question.getText(), answer.getText()); //создаем карточку
            cardList.add(card); //добавляем ее в список карт
            JFileChooser fileSave = new JFileChooser(); //создание объекта выбора места сохранения
            fileSave.showSaveDialog(frame); //показ диалога для сохранения
            saveFile(fileSave.getSelectedFile());   //сохранение в выбранное место
        }
    }

    public class NewMenuListener implements ActionListener{
        public void actionPerformed(ActionEvent ev){
            cardList.clear();   //очистка списка
            clearCard();    //сброс полей вопрос/ответ/фокус на поле вопрос
        }
    }

    private void clearCard(){
        question.setText("");   //очистка поля вопрос
        answer.setText(""); //очистка поля ответ
        question.requestFocus();    //фоку на поле вопрос
    }

    private void saveFile(File file){
        try {
            BufferedWriter writer = new BufferedWriter(new FileWriter(file));   //формирование буфера
            for(QuizCard card:cardList){    //проходим по всему списку карточек
                writer.write(card.getQuestion() + "/"); //формируем строку с вопросом
                writer.write(card.getAnswer() + "\n");  //формируем строку с ответом
            }
            writer.close(); //запись информации сформированной в буфере и его отчистка
        }catch (Exception ex){
            System.out.println("couldn`t write the cardList out");  //вывод в консоль - невозможно записать файл
            ex.printStackTrace();
        }
    }
}
