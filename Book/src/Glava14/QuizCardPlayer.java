package Glava14;

import java.util.*;
import java.awt.event.*;
import javax.swing.*;
import java.awt.*;
import java.io.*;

public class QuizCardPlayer {
    private JTextArea display;  //объявляем текстовое поле вопроса
    private JTextArea answer;   //объявляем текстовое поле ответа
    private ArrayList<QuizCard> cardList;   //объявляем лист-массив для хранения карточек
    private QuizCard currentCard;   //текущая карта
    private int currentCardIndex;   //хранит номер текущей карты
    private JFrame frame;   //фрейм окна
    private JButton nextButton; //кнопка
    private  boolean isShowAnswer;  //показан ли ответ в окне ответа

    public static void main(String[] args) {
        QuizCardPlayer reader = new QuizCardPlayer();   //создаем игру
        reader.go();    //запускаем игру
    }

    public void go(){
        //формируем gui
        frame = new JFrame("Quiz Card Player");     //создаем окно игры
        JPanel mainPanel = new JPanel();    //задаем панель
        Font bigFont = new Font("sanserif", Font.BOLD, 24); //задаем шрифт

        display = new JTextArea(10, 20);    //создаем текстовое поле
        display.setFont(bigFont);   //устанавливаем в нем шрифт

        display.setLineWrap(true);  //разрешаем стрелки прокрутки
        display.setEditable(false); //запрещаем изменять содержимое

        JScrollPane qScroller = new JScrollPane(display);   //объект скрол
        qScroller.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);    //разрешаем вертикальный скрол
        qScroller.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER); //запрещаем горизонтальный скрол
        nextButton = new JButton("Show Question");  //создаем кнопку
        mainPanel.add(qScroller);   //добавляем в панель скрол
        mainPanel.add(nextButton);  //добавляем в панель кнопку
        nextButton.addActionListener(new NextCardListener());   //следим за кнопкой при нажатии создаем объект NextCardListener

        JMenuBar menuBar = new JMenuBar();  //создаем меню бар
        JMenu fileMenu = new JMenu("File"); //часть меню бара - файл
        JMenuItem loadMenuItem = new JMenuItem("Load card set");    //подменю загрузка карт
        loadMenuItem.addActionListener(new OpenMenuListener()); //следим за меню открыть карты и создаем объект OpenMenuListener
        fileMenu.add(loadMenuItem); //добавляем в панель fileMenu подменю loadMenuItem
        menuBar.add(fileMenu);  //добавляем в панель menuBar меню fileMenu
        frame.setJMenuBar(menuBar); //устанавливаем menuBar во фрейм
        frame.getContentPane().add(BorderLayout.CENTER, mainPanel); //по центру
        frame.setSize(640, 500);    //размер фрейма
        frame.setVisible(true); //видимый
    }//закрыть метод go

    public class NextCardListener implements ActionListener{    //класс обработки вывода следующей карты
        @Override
        public void actionPerformed(ActionEvent ev) {
            if (isShowAnswer){
                //показываем ответ, так как вопрос уже был увиден
                display.setText(currentCard.getAnswer());   //устанавливем текст ответа из карты
                nextButton.setText("Next Card");    //ставим текст кнопка следующая карта
                isShowAnswer = false;   //показ ответа ложно
            }else {
                //показываем следующий вопрос
                if (currentCardIndex < cardList.size()){    //если есть еще карты

                    showNextCard(); //показываем следующюую

                }else {
                    //больше карточек нет!
                    display.setText("That was last card");
                    nextButton.setEnabled(false);
                }
            }
        }
    }

    public class OpenMenuListener implements ActionListener{
        @Override
        public void actionPerformed(ActionEvent ev) {
            JFileChooser fileOpen = new JFileChooser();     //формирование объекта поиска файло
            fileOpen.showOpenDialog(frame);     //показываем диалог
            loadFile(fileOpen.getSelectedFile());   //выбранный файл отправляем в loadFile
        }
    }

    private void loadFile(File file){ //загрузка файла

        cardList = new ArrayList<QuizCard>();   //создаем список карточек
        try {
            BufferedReader reader = new BufferedReader(new FileReader(file));   //осуществляем чтение через буфер
            String line = null; //строка line обнулена
            while ((line = reader.readLine()) != null){ //осуществляем чтение из буфера строки пока не кончатся
                makeCard(line); //создаем карту с параметрами сохраненной строки
            }
            reader.close(); //закрываем чтение
        }catch (Exception ex){  //обработка ошибок
            System.out.println("could`t read the card file");
            ex.printStackTrace();
        }
        //пришло время показать первую карточку
        showNextCard();
    }

    private void makeCard(String lineToParse){  //создание карты/принимаем сохраненную строку-требует десериализации
        String[] result = lineToParse.split("/");       //разделяем строку по символу "/"
        QuizCard card = new QuizCard(result[0], result[1]); //создаем карту с вопросом и ответом
        cardList.add(card); //добавляем карту с список
        System.out.println("made a card");  //сообщаем о создании карты
    }

    private void showNextCard(){    //отображаем следующую карту
        currentCard = cardList.get(currentCardIndex);   //берем карту с индексом currentCardIndex из списка cardList
        currentCardIndex++; //переходим к следующей карте
        display.setText(currentCard.getQuestion()); //показываем вопрос из карты
        nextButton.setText("Show Answer");  //меняем название кнопки на показать ответ
        isShowAnswer = true;    //показать ответ меняем на истино
    }
}//закрываем class
