package Glava14;

import java.io.ObjectStreamClass;

public class SerVerUID {
    public static void main(String[] args) {
        ObjectStreamClass c = ObjectStreamClass.lookup(Box.class);
        long serialID = c.getSerialVersionUID();
        System.out.println(serialID);
    }
}
