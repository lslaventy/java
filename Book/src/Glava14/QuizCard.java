package Glava14;

public class QuizCard {
    private String question;    //поле вопрос
    private String answer;  //поле ответ

    QuizCard(String q, String a){   //конструктор
        question = q;   //инициализация поля вопрос
        answer = a; //инициализация поля вопрос
    }

    public String  getQuestion(){
        return question;
    }   //получение поля вопрос
    public String getAnswer(){
        return answer;
    }   //получение поля ответ
}
