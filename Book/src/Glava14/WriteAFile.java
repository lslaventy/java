package Glava14;

import java.io.FileWriter;  //пакет для класса

public class WriteAFile {
    public static void main(String[] args) {
        try{
            FileWriter fileWriter = new FileWriter("Foo.txt"); //открываем файл
            fileWriter.write("Привет Фу!\n\tPrivate");  //напрямую записываем строку
            fileWriter.close(); //закрываем поток
        }catch (Exception ex){
            ex.printStackTrace();   //обрабатываем исключение
        }
    }
}
