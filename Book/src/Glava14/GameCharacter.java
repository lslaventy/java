package Glava14;

import java.io.Serializable;

public class GameCharacter implements Serializable {
    private int power;
    private String type;
    private String[] weapons;

    public GameCharacter(int p, String t, String[] w){  //конструктор класса принимающий его поля
        power = p;
        type = t;
        weapons = w;
    }

    public int getPower(){
        return power;
    }   //возвращает поле

    public String getType(){
        return type;
    }   //возвращает поле

    public String getWeapons(){ //возвращает поле
        String weaponList = "";
        for (String weapon : weapons) {
            weaponList += weapon + " ";
        }
        return weaponList;
    }
}
