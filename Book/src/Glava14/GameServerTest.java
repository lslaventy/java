package Glava14;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

public class GameServerTest {
    public static void main(String[] args) {
        //создаем трех персонажей
        GameCharacter one = new GameCharacter(50, "Эльф", new String[]{"лук", "меч", "кастет"});
        GameCharacter two = new GameCharacter(200, "Троль", new String[]{"голые руки", "большой топор"});
        GameCharacter three = new GameCharacter(120, "Маг", new String[]{"заклинания", "невидимость"});

        //Представьте себе код, который может изменить значение состояния персонажей

        try{
            ObjectOutputStream os = new ObjectOutputStream(new FileOutputStream("Game.ser"));   //открываем файл и поток к нему
            os.writeObject(one);    //сохраняем первый объект
            os.writeObject(two);    //сохраняем вротой объект
            os.writeObject(three);  //сохраняем третий объект
            os.close(); //закрываем поток
        }catch (Exception ex){
            ex.printStackTrace();   //обрабатываем исключение
        }
        //обнуляем указатели
        one = null;
        two = null;
        three = null;

        try {
            ObjectInputStream is = new ObjectInputStream(new FileInputStream("Game.ser"));  //открываем файл и поток
            GameCharacter oneRestore = (GameCharacter) is.readObject(); //читаем первый объект и восстанавливаем его тип
            GameCharacter twoRestore = (GameCharacter) is.readObject(); //читаем второй объект и восстанавливаем его тип
            GameCharacter threeRestore = (GameCharacter) is.readObject();   //читаем третий объект и восстанавливаем его тип
            is.close();//закрытие потока

            //выводим типы восстановленных объектов
            System.out.println("Тип первого: " + oneRestore.getType());
            System.out.println("Тип второго: " + twoRestore.getType());
            System.out.println("Тип третьего: " + threeRestore.getType());


        }catch (Exception ex){
            ex.printStackTrace();   //обработка исключений
        }
    }
}
