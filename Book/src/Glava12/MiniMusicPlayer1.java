package Glava12;

import javax.sound.midi.*;

public class MiniMusicPlayer1 {
    public static void main(String[] args) {
        try{
            Sequencer sequencer = MidiSystem.getSequencer();    //получили синтезатор
            sequencer.open();       //открыли синтезатор

            Sequence seq = new Sequence(Sequence.PPQ, 4);   //создаем новую последовательность - диск
            Track track = seq.createTrack();    //создаем трек в последовательности - на диске
            for(int i = 5; i < 61; i+=4){
                //заполняем трек событиями
                track.add(makeEvent(144, 1, i, 100, i));    //при открытии воспроизведения ноты - код 144
                track.add(makeEvent(128, 1, i, 100, i + 2));    //при закрытии воспроизведения ноты - код 128
            }
            sequencer.setSequence(seq); //установка диска в синтезатор
            sequencer.setTempoInBPM(220);   //установка темпа воспроизведения
            sequencer.start();  //запуск синтеатора на воспроизведение
        }catch (Exception ex){ex.printStackTrace();}    //обработка исключения
    }
    private static MidiEvent makeEvent(int comd, int chan, int one, int two, int trick){
        MidiEvent event = null; //формируем пустое событие, которое будем возвращать
        try{
            ShortMessage a = new ShortMessage();    //создание сообщения для синтезатора
            a.setMessage(comd, chan, one, two); //запись сообщения
            event = new MidiEvent(a, trick);    //формирование события
        }catch (Exception e){e.printStackTrace();}  //обработка исключения
        return event;   //возврат события
    }
}
