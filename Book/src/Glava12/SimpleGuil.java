package Glava12;

import javax.swing.*;
import java.awt.*;

public class SimpleGuil {
    public static void main(String[] args) {
        JFrame frame = new JFrame("Первое мое окно");
        JButton button = new JButton("click me");

        Font forButton = new Font("Times New Roman", Font.BOLD, 30);
        button.setFont(forButton);

        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.getContentPane().add(button);

        frame.setSize(300, 300);
        frame.setVisible(true);
    }

    public void changeIt(JButton button){
        button.setText("I`ve been clicked!");
    }
}
