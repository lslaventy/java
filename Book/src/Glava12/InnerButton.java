package Glava12;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class InnerButton {

    JFrame frame;   //объявление фрейма - окна
    JButton b;  //объявление кнопки

    public static void main(String[] args) {
        InnerButton gui = new InnerButton();    //создаем приложение
        gui.go();   //запускаем приложение
    }
    public void go(){
        frame = new JFrame();   //создаем фрейм - окно
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);   //устанавливаем настройки фрейма для его закрытия

        b = new JButton("A");   //создаем кнопку с названием A
        b.addActionListener(new BListner());    //добавляем кнопку к слушателю и передаем объект для вызова действия

        frame.getContentPane().add(BorderLayout.SOUTH, b);  //располагаем кнопку на панели внизу
        frame.setSize(200, 100);    //устанавливаем размер фрейма
        frame.setVisible(true); //делаем фрейм видимым
    }
    class BListner implements ActionListener{
        @Override
        public void actionPerformed(ActionEvent e) {    //
            if(b.getText().equals("A")){        //проверка названия клавиш
                b.setText("B");     //если совпало то поменять текст на кнопке
            }else {
                b.setText("A");     //нет то поставить текст
            }
        }
    }
}
