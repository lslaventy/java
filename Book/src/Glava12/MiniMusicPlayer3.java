package Glava12;

import javax.sound.midi.*;
import javax.swing.*;
import java.awt.*;

public class MiniMusicPlayer3 {
    static JFrame f = new JFrame("Мой первый музыкальный клип");    //создаем фрейм - окно с надписью
    static MyDrawPanel ml;  // объявляем статическую переменнную пользовательского типа

    public static void main(String[] args) {
        MiniMusicPlayer3 mini = new MiniMusicPlayer3(); //создаем объект приложения
        mini.go();  //запускаем приложение
    }

    //произведем настройки графического интерфейса
    public  void setUpGui(){
        ml = new MyDrawPanel(); //создаем графическую панель
        f.setContentPane(ml);   // добавляем графическую панель во фрейм
        f.setBounds(30, 30, 300, 300);  // устанавливаем положение фрейма и размер окна
        f.setVisible(true); //делаем окно видимым
    }

    //функция запуска программы
    public void go(){
        setUpGui(); //устанавливаем настройки графическогго интерфейса

        try{
            //запускаем синтезатор
            Sequencer sequencer = MidiSystem.getSequencer();    //получаем синтезетор
            sequencer.open();   //открываем синтезатор
            sequencer.addControllerEventListener(ml, new int[] {127});  //добавляем слушателя на событие 127 для графической панели
            Sequence seq = new Sequence(Sequence.PPQ, 4);   //создаем диск
            Track track = seq.createTrack();    //надиске размещаем трек

            int r = 0;
            for(int i = 0; i < 60; i += 4){
                r = (int)((Math.random() * 50 ) + 1);
                //играем ноты
                track.add(makeEvent(144, 1, r, 100, i));    //начало воспроизведения
                track.add(makeEvent(176, 1, 127, 0, i)); //включение события с обращением к классу MyDrawPanel
                track.add(makeEvent(128, 1, r, 100, i + 2));    //окончание воспроизведения
            }

            sequencer.setSequence(seq); //вставляем диск в синтезатор
            sequencer.start();  //запускаем
            sequencer.setTempoInBPM(120);   //выставляем темп
        }catch (Exception ex){ex.printStackTrace();}    //обработка исключения
    }

    //метод создающий событие
    public MidiEvent makeEvent(int comd, int chan, int one, int two, int tick){
        MidiEvent event = null;
        try{
            ShortMessage a = new ShortMessage();    //создаем сообщение
            a.setMessage(comd, chan, one, two); //устанавливаем сообщение
            event = new MidiEvent(a, tick);     //записываем событие

        }catch (Exception e){}  //обработка исключения
        return event;   //возврат события
    }

    public class MyDrawPanel extends JPanel implements ControllerEventListener {
        //класс работающий с событиями JPanel и ControllerEventListener
        //графической частью и звуковой
        boolean msg = false;

        //музыкальная часть
        @Override
        public void controlChange(ShortMessage event) {
            msg = true;
            repaint();  //перерисовывает графические фигуры
        }

        //графическая часть
        public void paintComponent(Graphics g){
            if(msg){
                Graphics2D g2 = (Graphics2D) g;
                //выбираем цвет
                int r = (int) (Math.random() * 250);    //красный
                int gr = (int) (Math.random() * 250);   //знленый
                int b = (int) (Math.random() * 250);    //синиц

                g.setColor(new Color(r,gr,b));  //установка цвета

                //рисуем фигуру
                int ht = (int) ((Math.random() * 120) + 10);    //высота рисунка
                int width = (int) ((Math.random() * 120) + 10); //ширина рисука
                int x = (int) ((Math.random() * 40) + 10);//начальная X
                int y = (int) ((Math.random() * 40) + 10);  //начальная Y
                g.fillRect(x, y, ht, width);    //рисуем прямоугольник
                msg = false;
            }
        }
    }
}
