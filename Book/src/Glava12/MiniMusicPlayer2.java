package Glava12;

import javax.sound.midi.*;

public class MiniMusicPlayer2 implements ControllerEventListener{
    public static void main(String[] args) {
        MiniMusicPlayer2 mini = new MiniMusicPlayer2();
        mini.go();
    }

    private void go(){
        try{
            Sequencer sequencer = MidiSystem.getSequencer();    //получаем синтезатор
            sequencer.open();   //открываем синтезатор

            int[] eventIWant = {127};   //создаем "эвент" последовательность интересующих событий (в частности №127)
            sequencer.addControllerEventListener(this, eventIWant); //добавляем синтезатор как слушателя
            //для этого объекта и интересующее событие #127

            Sequence seq = new Sequence(Sequence.PPQ, 4);   //создаем последовательность
            Track track = seq.createTrack();    //создаем трек в последовательности

            for(int i = 5; i < 60; i+=4){
                track.add(makeEvent(144,  i, 100, i));  //144 начало проигрывания ноты

                track.add(makeEvent(176, 127,0, i)); //запуск события -код 176 событие ControllerEvent

                track.add(makeEvent(128,  i, 100, 2)); //128 конец проигрывания ноты
            }
            sequencer.setSequence(seq); //устанавливаем последовательность в синтезатор
            sequencer.setTempoInBPM(220);   //установка темпа воспроизведения
            sequencer.start();  //запуск синтезатора
        }catch (Exception e){e.printStackTrace();}  //обработка исключения
    }

    public void controlChange(ShortMessage event){  //выполнение события по коду 176
        System.out.println("ля");
    }

    private MidiEvent makeEvent(int comd, int one, int two, int tick){
        MidiEvent event = null;
        try {
            ShortMessage a = new ShortMessage();
            a.setMessage(comd, 1, one, two);
            event = new MidiEvent(a, tick);
        }catch (Exception e){
            e.printStackTrace();
        }
        return event;
    }
}