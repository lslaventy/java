package Box;

import javax.sound.midi.*;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.*;
import java.util.*;

public class BeatBox {

    private ArrayList<JCheckBox> checkBoxList;  //объявляем лист массив с данными определенного типа
    private Sequencer sequencer;    //определяем синтезатор
    private Sequence sequence;  //определяем последовательность - диск
    private Track track;    //определяем трек
    private JFrame theFrame;

    //задаем массив строк с названиями инструментов
    private String[] instrumentNames = {"Bass Drum", "Closed Hi-Hat", "Open Hi-Hat", "Acoustic Snare", "Crash Cymbal",
    "Hand Clap", "High Tom", "Hi Bongo", "Maracas", "Whistle", "Low Conga", "Cowbell", "Vibraslap", "Low-mid Tom",
    "High Agogo", "Open Hi Conga"};
    //создаем массив с кодами инструментов огласованноми с их названиями
    private int[] instruments = {35, 42, 46, 38, 49, 39, 50, 60, 70, 72, 64, 56, 58, 47, 67, 63};

    public static void main(String[] args) {
        new BeatBox().buildGUI();   //создаем приложение и запускаем программу
    }
    private void buildGUI(){
        theFrame = new JFrame("Cyber BeatBox");  //создаем окно
        theFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);    //определяем настройки окна при закрытии
        BorderLayout layout = new BorderLayout();   //определяем объект управления контентом
        JPanel background = new JPanel(layout); //создаем панель для заднего фона
        background.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));  //

        checkBoxList = new ArrayList<>();   //создали массив лист
        Box buttonBox = new Box(BoxLayout.Y_AXIS);  //создаем объект управления контентом настраиваем его на вертикальное добавление контента

        JButton start = new JButton("Start");   //создаем кнопку старт
        start.addActionListener(new MyStartListener()); //добавляем кнопке слушетеля в объекте MyStartListener
        buttonBox.add(start);   //добавляем в контролер контента кнопку старт

        JButton stop = new JButton("Stop"); //создаем кнопку стоп
        stop.addActionListener(new MyStopListener());   //добавляем слушателя
        buttonBox.add(stop);    //кладем в панель в объект управления контентом

        JButton upTempo = new JButton("Tempo Up");  //создаем кнопку увеличения темпа
        upTempo.addActionListener(new MyUpTempoListener()); //добавим слушателя с реализацией в объекте MyUpTempoListener
        buttonBox.add(upTempo); //добавим в панель

        JButton downTempo = new JButton("Tempo Down");  //создадим кнопку снижения темпа
        downTempo.addActionListener(new MyDownTempoListener()); //добавим слушателя с реализацией в объекте
        buttonBox.add(downTempo);   //добавим кнопку в объект управления контентом

//        JButton serializeIt = new JButton("SerializeIt"); //создаем кнопку
//        serializeIt.addActionListener(new MySendListener());  //подключаем слушатель и действие на объект
//        buttonBox.add(serializeIt);   //добавляем кнопку на панель кнопок

        JButton serializeIt = new JButton("SerializeIt");   //создаем кнопку
        serializeIt.addActionListener(new SaveMenuListener());      //добавляем слушателя и действие на объект
        buttonBox.add(serializeIt); //добавляем кнопке на панель кнопок

//        JButton restore = new JButton("Restore"); //создаем кнопку
//        restore.addActionListener(new MyReadListener());  //добавляем слушателя и действи на объект
//        buttonBox.add(restore);   //добавляем кнопку в панель кнопок

        JButton restore = new JButton("Restore");   //создаем кнопку
        restore.addActionListener(new OpenMenuListener());  //добавляем слушателя и действие на объект
        buttonBox.add(restore); //добаляем кнопку в панель кнопок

        Box nameBox = new Box(BoxLayout.Y_AXIS);    //создаем новую панель с вертикальной разверткой
        for (int i = 0; i < 16; i++){   //идем по 16 инструментам
            nameBox.add(new Label(instrumentNames[i])); //присваиваем имя инструмента каждой из 16 строк
        }

        background.add(BorderLayout.EAST, buttonBox);   //располагаем панель кнопок на панели фона
        background.add(BorderLayout.WEST, nameBox);     //располагаем панель имен инструментов на панели фона

        theFrame.getContentPane().add(background);      //располагаем панель фона в панели контента нашего фрейма

        GridLayout grid = new GridLayout(16, 16);   //создаем сетку 16х16
        grid.setVgap(1);    //устанавливает зазор между компонентами вертикальный
        grid.setHgap(2);    //устанавливает зазор между компонентами горизонтальный
        JPanel mainPanel = new JPanel(grid);    //создаем панель с сеткой
        background.add(BorderLayout.CENTER, mainPanel); //размещаем панель с сеткой в центральной секции фрейма

        for (int i = 0; i < 256; i++){
            JCheckBox c = new JCheckBox();  //создаем 256 чекбоксов
            c.setSelected(false);   //устанавливаем их выбор в ложное полоение
            checkBoxList.add(c);    //добавляем в список(массив) наш чекбокс в цикле
            mainPanel.add(c);   //располагаем чекбокс на панеле (поле 16х16)
        }

        setUpMidi();    //проводим натройку миди

        theFrame.setBounds(50, 50, 300, 300);   //устанавливаем размеры окна
        //theFrame.pack();    //расчет предпочтительного размера окна при запуске
        theFrame.setVisible(true);  //делаем окно видимым
    }

    private void setUpMidi(){
        try{
            sequencer = MidiSystem.getSequencer();  //получаем миди
            sequencer.open();   //открываем его
            sequence = new Sequence(Sequence.PPQ, 4);   //получаем синтезатор
            track = sequence.createTrack(); //создаем дорожку
            sequencer.setTempoInBPM(120);   //задаем темп
        }catch (Exception ex){ex.printStackTrace();}
    }
    private void buildTrackAndStart(){  //строим трек и воспроизводим
        int[] trackList;    //объявляем массив

        sequence.deleteTrack(track);    //удаляем предидущий трек в синтезаторе
        track = sequence.createTrack(); //создаем новый трек

        for (int i = 0; i < 16; i++){
            trackList = new int[16];    //создаем массив на 16 тактов треклист

            int key = instruments[i];   //берем инструмент из массива инструментов

            for (int j = 0; j < 16; j++){
                JCheckBox jc = checkBoxList.get(j + (16 * i)); //берем из списка чекбоксов каждый из 16 на 16
                if(jc.isSelected()){    //определяем если он выбран
                    trackList[j] = key; //добовляем в треклист номер инструмента
                }else {
                    trackList[j] = 0;   //добавляем 0
                }
            }

            makeTracks(trackList);  //создаем трек
            track.add(makeEvent(176, 1, 127, 0, 16));   //добавили параметры
        }

        track.add(makeEvent(192, 9, 1, 0, 15));     //добавили параметры
        try {
            sequencer.setSequence(sequence);    //загрузили диск в синтезатор
            sequencer.setLoopCount(sequencer.LOOP_CONTINUOUSLY);//задаем повторение цикла до прерывания
            sequencer.start();  //запускаем синтезатор
            sequencer.setTempoInBPM(120);   //установка темпа
        }catch (Exception ex){ex.printStackTrace();}
    }
    public class MyStartListener implements ActionListener{//нажали кнопку запуска воспроизведения

        @Override
        public void actionPerformed(ActionEvent e) {
            buildTrackAndStart();
        }//запускаем функцию сборки трека и воспроизведения
    }

    public class MyStopListener implements ActionListener{  //нажали кнопку остановки воспроизведения

        @Override
        public void actionPerformed(ActionEvent e) {
            sequencer.stop();
        }   //даем команду синтезатору остановиться
    }
    public class MyUpTempoListener implements ActionListener{   //повышение темпа

        @Override
        public void actionPerformed(ActionEvent e) {
            float tempoFactor = sequencer.getTempoFactor();
            sequencer.setTempoFactor((float)(tempoFactor * 1.03));  //увеличиваем темп воспроизведения
        }
    }
    public class MyDownTempoListener implements ActionListener{ //понижение темпа воспроизведения

        @Override
        public void actionPerformed(ActionEvent e) {
            float tempoFactor = sequencer.getTempoFactor();
            sequencer.setTempoFactor((float)(tempoFactor * .97));   //снижаем темп воспроизведения
        }
    }
    private void makeTracks(int[] list){
        for (int i = 0; i < 16; i++){
            int key = list[i];

            if(key != 0){
                track.add(makeEvent(144, 9, key, 100, i));
                track.add(makeEvent(128, 9, key, 100, i + 1));
            }
        }
    }
    private   MidiEvent makeEvent(int comd, int chan, int one, int two, int tick){
        MidiEvent event = null;
        try {
            ShortMessage a = new ShortMessage();
            a.setMessage(comd, chan, one, two);
            event = new MidiEvent(a, tick);
        }catch (Exception ex){ex.printStackTrace();}
        return event;
    }

    public class MySendListener implements ActionListener{  //сохранение без окна
        @Override
        public void actionPerformed(ActionEvent e) {
            boolean[] checkboxState = new boolean[256]; //булев массив на все 256 элементов

            for (int i = 0; i < 256;i++){
                JCheckBox check = (JCheckBox) checkBoxList.get(i);  //считываем и
                if(check.isSelected()){
                    checkboxState[i] = true;    //копируем в новый массив
                }
            }
            try{
                FileOutputStream fileStream = new FileOutputStream(new File("Checkbox.ser"));   //создаем новый файл
                ObjectOutputStream os = new ObjectOutputStream(fileStream); //выводим в поток
                os.writeObject(checkboxState);  //записываем чекбокс с треками
            }catch (Exception ex){
                ex.printStackTrace();
            }
        }
    }

    public class MyReadListener implements ActionListener{  //для чтения
        @Override
        public void actionPerformed(ActionEvent e) {

            boolean[] checkboxState = null; //обнулили наш чек бокс
            try{
                FileInputStream fileIn = new FileInputStream(new File("Checkbox.ser")); //открываем файл
                ObjectInputStream is = new ObjectInputStream(fileIn);   //получаем доступ через поток
                checkboxState = (boolean[])  is.readObject();   //копируем в чек бокс
            }catch (Exception ex){
                ex.printStackTrace();
            }
            for (int i = 0; i < 256; i++){
                JCheckBox check = (JCheckBox) checkBoxList.get(i);//берем каждый из 256 чеков
                assert checkboxState != null;
                if(checkboxState[i]){   //проверяем на истинность чекбоксы и если да то
                    check.setSelected(true);    //устанавливаем в наш чеклист истино
                }else {
                    check.setSelected((false)); //или отрицательно
                }
            }
            sequencer.stop();   //остановили синтезатор со старым треком
            buildTrackAndStart();   //смонтировали и запустили с новым треком
        }

    }
    //то же сохранение и чтение только с диалоговым окном
    public class OpenMenuListener implements ActionListener{
        @Override
        public void actionPerformed(ActionEvent e) {
            JFileChooser fileOpen = new JFileChooser();
            fileOpen.showOpenDialog(theFrame);
            loadFile(fileOpen.getSelectedFile());
        }
    }
    private void loadFile(File file){
        boolean[] checkboxState = null;
        try{
            FileInputStream fileIn = new FileInputStream(file);
            ObjectInputStream is = new ObjectInputStream(fileIn);
            checkboxState = (boolean[])  is.readObject();
        }catch (Exception ex){
            ex.printStackTrace();
        }
        for (int i = 0; i < 256; i++){
            JCheckBox check = (JCheckBox) checkBoxList.get(i);
            assert checkboxState != null;
            if(checkboxState[i]){
                check.setSelected(true);
            }else {
                check.setSelected((false));
            }
        }
        sequencer.stop();
        buildTrackAndStart();
    }

    public class SaveMenuListener implements ActionListener{
        @Override
        public void actionPerformed(ActionEvent e) {
            JFileChooser fileSave = new JFileChooser();
            fileSave.showSaveDialog(theFrame);
            saveFile(fileSave.getSelectedFile());
        }
    }
    private void saveFile(File file){
        boolean[] checkboxState = new boolean[256];
        for (int i = 0; i < 256;i++){
            JCheckBox check = (JCheckBox) checkBoxList.get(i);
            if(check.isSelected()){
                checkboxState[i] = true;
            }
        }
        try{
            FileOutputStream fileStream = new FileOutputStream(file);
            ObjectOutputStream os = new ObjectOutputStream(fileStream);
            os.writeObject(checkboxState);
        }catch (Exception ex){
            ex.printStackTrace();
        }
    }
}
