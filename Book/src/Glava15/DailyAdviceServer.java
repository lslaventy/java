package Glava15;

import java.io.*;
import java.net.*;

public class DailyAdviceServer {
    //создаем массив строк с советами дня
    private String[] adviceList = {"Ешьте меньшими порциями", "Купите облегающие джинсы. Нет они не делают вас полнее",
            "Два слова: не годится", "Будьте честны хотя бы сегодня. Скажите своему начальнику все, что вы *на самом " +
            "деле* о нем думаете.", "Возможно, вам стоит подобрать другую прическу."};

    public void go(){
        try{
            ServerSocket serverSocket = new ServerSocket(4242); //создаем сервер слушающий порт 4242

            while (true){
                Socket sock = serverSocket.accept();    //при получении сообщения через порт создать новое соединение на новом порту

                PrintWriter writer = new PrintWriter(sock.getOutputStream());   //направление потока на сериализатор Writer
                String advice = getAdvice();    //получение строки из массива
                writer.println(advice);     //отправка строки через сериализатор в выходной поток на сокет
                writer.close(); //закрыть сериализатор т.е. освобождаем память от объекта
                System.out.println(advice); //показать что мы отправили
                System.out.println(sock.getLocalPort());    //показать наш локальный порт
                System.out.println(sock.getPort()); //показать порт устройства кому отправили
            }
        }catch (IOException ex){
            ex.printStackTrace();   //вывод стека ошибок
        }
    }
    private String getAdvice(){
        int random = (int)(Math.random() * adviceList.length);  //создание случайного числа в пределах размера массива
        return adviceList[random];  //вернем случайную строку
    }

    public static void main(String[] args) {
        DailyAdviceServer server = new DailyAdviceServer(); //создаем сервер
        server.go();    //зспустим сервер
    }
}
