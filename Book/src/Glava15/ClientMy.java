package Glava15;

import java.io.*;
import java.net.*;

public class ClientMy {
    public void go(){
        try {
            Socket socket = new Socket("127.0.0.1", 4242);

            InputStreamReader reader = new InputStreamReader(socket.getInputStream());
            BufferedReader advice = new BufferedReader(reader);

            System.out.println("Совет: " + advice.readLine());
            advice.close();
        }catch (IOException ex){
            ex.printStackTrace();
        }
    }

    public static void main(String[] args) {
        ClientMy client = new ClientMy();
        client.go();
    }
}
