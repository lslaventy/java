package Glava15;

import java.io.*;
import java.net.*;

public class DailyAdviceClient {
    public void go(){
        try{
            Socket s = new Socket("127.0.0.1", 4242);   //создаем соединение с сервером

            InputStreamReader streamReader = new InputStreamReader(s.getInputStream()); //получаем из соединения входной поток - сериализатор
            BufferedReader reader = new BufferedReader(streamReader);   //направляем поток в буффер

            String advice = reader.readLine();  //считываем строку из буффера
            System.out.println("Сегодня ты должен:" + advice);  //печатем строку из буффера
            System.out.println(s.getPort() + " " + s.getLocalPort());   //печатаем порты новый и локальный

            reader.close(); //закрываем буффер
            streamReader.close();   //закрываем поток
            s.close();  //закрываем соединение
        }catch (IOException ex){
            ex.printStackTrace();   //вывод ошибок
        }
    }

    public static void main(String[] args) {
        DailyAdviceClient client = new DailyAdviceClient(); //создаем клиент
        client.go();    //запускаем клиент
    }
}
