package Glava13;

import javax.swing.*;
import java.awt.*;

public class Panel1 {
    public static void main(String[] args) {
        Panel1 gui = new Panel1();
        gui.go();
    }
    private void go(){
        JFrame frame = new JFrame();
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        JPanel panel =new JPanel();
        panel.setBackground(Color.darkGray);

        JButton button = new JButton("shock me");
        JButton buttonTwo = new JButton("blees");
        JButton buttonThree = new JButton("huh?");

//        BoxLayout boxLayout = new BoxLayout(panel, BoxLayout.Y_AXIS);
//        panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));

        panel.add(button);
        panel.add(buttonTwo);
        panel.add(buttonThree);

        frame.setContentPane(panel);
//        frame.getContentPane().add(BorderLayout.EAST, panel);

        frame.setSize(250, 200);
        frame.setVisible(true);
    }
}
