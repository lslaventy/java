package Glava13;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Panel2 {
    JTextField field;
    JTextArea textArea;
    public static void main(String[] args) {
        Panel2 gui = new Panel2();
        gui.go();
    }
    private void go(){
        JFrame frame = new JFrame();
        field = new JTextField("Ваше имя",20);
        textArea = new JTextArea(10, 20);
        JScrollPane scroller = new JScrollPane(textArea);
        textArea.setLineWrap(true);
        scroller.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
        scroller.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);

        textArea.setText("не все потерявшиеся - бродяги");
        textArea.append("кнопка нажата");
        textArea.selectAll();
        textArea.requestFocus();

        field.addActionListener(new MyTextField());
        field.selectAll();
        field.requestFocus();
        field.setSize(10, 10);


        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.getContentPane().add(BorderLayout.NORTH, scroller);
        frame.setSize(200, 200);
        frame.setVisible(true);
    }
    class MyTextField implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
            System.out.println(field.getText());
        }
    }
}
