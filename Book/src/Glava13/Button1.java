package Glava13;

import javax.swing.*;
import java.awt.*;

public class Button1 {
    public static void main(String[] args) {
        Button1 gui = new Button1();
        gui.go();
    }
    public void go(){
        JFrame frame = new JFrame();
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
//        JButton button = new JButton("click like you mean it");
//        Font bigFont = new Font("serif", Font.BOLD, 28);
//        button.setFont(bigFont);
//        frame.getContentPane().add(BorderLayout.NORTH, button);
        //frame -  окно
        //getContentPane() - это панель JPanel
        //add - добавим компоеннт на панель
        //BorderLayout.EAST - диспетчер компоновки размещает в поле EAST
        //button - что мы размещаем, а именно кнопку

        JButton north = new JButton("north");
        JButton south = new JButton("south");
        JButton west = new JButton("west");
        JButton east = new JButton("east");
        JButton center= new JButton("center");
        frame.getContentPane().add(BorderLayout.NORTH, north);
        frame.getContentPane().add(BorderLayout.SOUTH, south);
        frame.getContentPane().add(BorderLayout.EAST, east);
        frame.getContentPane().add(BorderLayout.WEST, west);
        frame.getContentPane().add(BorderLayout.CENTER, center);
        frame.setSize(300, 300);
        frame.setVisible(true);

    }
}
